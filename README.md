# README #

 A basic app demonstrating the basics of SQLite with Android. The app implements SQLite database with queries and finding unique data with the Cursor. App will perform all storage queries with SQLite.
      
### SQLite Database ###
* Save
* Load
* Update
* Delete

### Other Examples Include ###
* Using Explicit Intents to pass data to next activity
* Creating menus for top action bar

### Official Documentation ###
* [SQLite](https://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html)

* [Intents](https://developer.android.com/reference/android/content/Intent.html)

* [Menus](https://developer.android.com/guide/topics/ui/menus.html)


![Screen Shot 2017-06-08 at 01.37.52.png](https://bitbucket.org/repo/7EzXepE/images/987709687-Screen%20Shot%202017-06-08%20at%2001.37.52.png)