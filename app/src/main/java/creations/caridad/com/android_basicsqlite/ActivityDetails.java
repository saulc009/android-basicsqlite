package creations.caridad.com.android_basicsqlite;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityDetails extends AppCompatActivity {

    EditText editTextFirstName;
    EditText editTextLastName;
    EditText editTextAgeName;
    Button buttonUpdate;
    Button buttonDelete;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        context = this;

        //XML Ui
        editTextFirstName = (EditText) findViewById(R.id.editTextDetailFirstName);
        editTextLastName = (EditText) findViewById(R.id.editTextDetailLastName);
        editTextAgeName = (EditText) findViewById(R.id.editTextDetailAge);
        buttonDelete = (Button) findViewById(R.id.buttonDelete);
        buttonUpdate = (Button) findViewById(R.id.buttonUpdate);

        // call methods here
        loadData();
        deleteUser();
        updateUser();
    }

    private void loadData() {
        // get items from intent
        Intent intent = getIntent();
        setResult(RESULT_OK, intent);

        // set user saved text
        editTextFirstName.setText(intent.getExtras().getString("firstname"));
        editTextLastName.setText(intent.getExtras().getString("lastname"));
        editTextAgeName.setText(intent.getExtras().getString("age"));
    }

    private void updateUser() {
        // get items from intent
        final Intent intent = getIntent();
        setResult(RESULT_OK, intent);
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if db exists
                SQLiteDatabase userDB = context.openOrCreateDatabase("Users", MODE_PRIVATE, null);
                // delete user data
                userDB.execSQL("DELETE FROM users WHERE firstname = '" + intent.getExtras().getString("firstname") + "'");
                userDB.execSQL("DELETE FROM users WHERE lastname = '" + intent.getExtras().getString("lastname") + "'");
                userDB.execSQL("DELETE FROM users WHERE age = '" + intent.getExtras().getString("age") + "'");

                // Save updated user data
                // create table
                userDB.execSQL("CREATE TABLE IF NOT EXISTS users (firstname VARCHAR, lastname VARCHAR, age Int(3))");
                // insert user data to table
                userDB.execSQL("INSERT INTO users (firstname, lastname, age) VALUES (" + "'" + editTextFirstName.getText().toString() + "'" + ", " + "'" + editTextLastName.getText().toString() + "'" + "," + "'" + editTextAgeName.getText().toString() + "'" + ")");
                // send toast
                Toast.makeText(context, "Update Success", Toast.LENGTH_SHORT).show();
                // go back
                finish();
            }
        });
    }

    private void deleteUser() {
        // get items from intent
        final Intent intent = getIntent();
        setResult(RESULT_OK, intent);

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if db exists
                SQLiteDatabase userDB = context.openOrCreateDatabase("Users", MODE_PRIVATE, null);
                // delete user data
                userDB.execSQL("DELETE FROM users WHERE firstname = '" + intent.getExtras().getString("firstname") + "'");
                userDB.execSQL("DELETE FROM users WHERE lastname = '" + intent.getExtras().getString("lastname") + "'");
                userDB.execSQL("DELETE FROM users WHERE age = '" + intent.getExtras().getString("age") + "'");
                // send toast
                Toast.makeText(context, "Delete Success", Toast.LENGTH_SHORT).show();
                // go back
                finish();
            }
        });
    }
}
