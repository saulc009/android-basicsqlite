package creations.caridad.com.android_basicsqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityForm extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        context = this;

        // call methods here
        saveNewUser();
    }

    private void saveNewUser() {
        // XML UI
        final EditText enterFirstName = (EditText) findViewById(R.id.editTextFirstName);
        final EditText enterLastName = (EditText) findViewById(R.id.editTextLastName);
        final EditText enterAgeName = (EditText) findViewById(R.id.editTextage);
        Button buttonSave = (Button) findViewById(R.id.buttonSave);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // check if edit text fields are empty
                if (enterFirstName.getText().toString().trim().length() != 0 && enterLastName.getText().toString().trim().length() != 0 && enterAgeName.getText().toString().trim().length() != 0) {
                    // save to sql
                    try {

                        // check if db exists
                        SQLiteDatabase userDB = context.openOrCreateDatabase("Users", MODE_PRIVATE, null);
                        // create table
                        userDB.execSQL("CREATE TABLE IF NOT EXISTS users (firstname VARCHAR, lastname VARCHAR, age Int(3))");
                        // insert user data to table
                        userDB.execSQL("INSERT INTO users (firstname, lastname, age) VALUES (" + "'" + enterFirstName.getText().toString() + "'" + ", " + "'" + enterLastName.getText().toString() + "'" + "," + "'" + enterAgeName.getText().toString() + "'" + ")");

                        // go back
                        finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "Please fill everything out", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}