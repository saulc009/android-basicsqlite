package creations.caridad.com.android_basicsqlite;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import creations.caridad.com.android_basicsqlite.UsersClass.ClassUsers;

public class MainActivity extends AppCompatActivity {

    Context context;
    ListView listView = null;
    private ArrayAdapter<ClassUsers> arrayAdapter = null;
    private ArrayList<ClassUsers> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        // XML ui
        listView = (ListView) findViewById(R.id.listview);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // clear arraylist, used to get rid of redundant data
        arrayList.clear();
        // call methods here
        loadAndDisplayUserData();
    }

    private void loadAndDisplayUserData() {
        try {
            // check if db exists
            SQLiteDatabase userDB = context.openOrCreateDatabase("Users", MODE_PRIVATE, null);
            // create table
            userDB.execSQL("CREATE TABLE IF NOT EXISTS users (firstname VARCHAR, lastname VARCHAR, age Int(3))");
            // select users
            Cursor cursor = userDB.rawQuery("SELECT * FROM users", null);

            // select indexes
            int firstNameIndex = cursor.getColumnIndex("firstname");
            int lastNameIndex = cursor.getColumnIndex("lastname");
            int ageNameIndex = cursor.getColumnIndex("age");

            // move curser to the top
            cursor.moveToFirst();

            while (cursor != null) {
                // add data from DB
                arrayList.add(new ClassUsers(cursor.getString(firstNameIndex), cursor.getString(lastNameIndex), cursor.getString(ageNameIndex)));
                // move to next item in DB
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // set all users data and adapter
        arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line, arrayList);
        listView.setAdapter(arrayAdapter);

        // click listener for listview
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.i("test", String.valueOf(arrayList.get(i).getFirstName()));

                // Explicit Intent
                // send our selected listview index user data to activitydetails
                Intent intent = new Intent(context, ActivityDetails.class);
                intent.putExtra("firstname", arrayList.get(i).getFirstName());
                intent.putExtra("lastname", arrayList.get(i).getLastName());
                intent.putExtra("age", arrayList.get(i).getAge());
                startActivity(intent);
            }
        });
    }

    // add menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mainactivity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // add action buttons
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.buttonAdd:
                // go to add form activity
                Intent intent = new Intent(this, ActivityForm.class);
                startActivity(intent);
                break;
            default:
                return false;
        }
        return true;
    }
}